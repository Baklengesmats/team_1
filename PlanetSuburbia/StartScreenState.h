#pragma once
#include "Incl.h";

using namespace std;

class StartScreenState
{
public:
	sf::Sprite sprite;

	StartScreenState();
	~StartScreenState();

	void runStartScreenState(sf::RenderWindow &window);
};


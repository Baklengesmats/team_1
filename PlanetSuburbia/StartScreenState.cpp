#include "StartScreenState.h"


StartScreenState::StartScreenState()
{
	sprite.setTextureRect(sf::IntRect(0, 0, 100, 200));
}


StartScreenState::~StartScreenState()
{
}

void StartScreenState::runStartScreenState(sf::RenderWindow &window)
{
	sprite.setPosition(100, 100);
	window.draw(sprite);
}
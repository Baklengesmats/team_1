#include "Incl.h";
#include "StateManager.h"
#include "StartScreenState.h"

using namespace std;

int main ()
{
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "SFML works!", sf::Style::Titlebar | sf::Style::Close);

	sf::Clock clock;


	sf::Texture T_StartScreenState;
	/*T_StartScreenState.loadFromFile("Gameworld 1 example 1.png", sf::IntRect(0, 0, 160, 160));*/
	if (!T_StartScreenState.loadFromFile("Start1.png", sf::IntRect(0, 0, 160, 160)))
	{
		cout << "Error! Couldn't load texture" << endl;
	}

	StartScreenState StartScreenState;
	StartScreenState.sprite.setTexture(T_StartScreenState);

	StateManager StateManager;

	const float targettime = 1.0f / 60.0f;
	float accumulator = 0.0f;
	float frametime = 0.0f;

	while (window.isOpen())
	{
		sf::Time deltatime = clock.restart();
		frametime = std::min(deltatime.asSeconds(), 0.1f);
		if (frametime > 0.1f)
			frametime = 0.1f;
		accumulator += frametime;
		while (accumulator > targettime)
		{
			accumulator -= targettime;

			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					window.close();
				}
			}
		}
		window.clear(sf::Color(0x011, 0x52, 0x33, 0xff));
		StateManager.startGame(window);
		//StartScreenState.runStartScreenState(window);

		//window.draw(StartScreenState.sprite);

		window.display();
	}
	return 0;
}
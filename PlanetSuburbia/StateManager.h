#pragma once
#include "StartScreenState.h"

class StateManager
{
public:
	StateManager();

	void startGame(sf::RenderWindow &window);
	void callStartScreenState(sf::RenderWindow &window);

};
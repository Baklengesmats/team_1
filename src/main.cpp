
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <iostream>

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif

#include 
int main(int argc, char** argv)
{
	sf::RenderWindow window;
	window.create(sf::VideoMode(1024, 1000), "sfml-app", sf::Style::Titlebar | sf::Style::Close);
	if (!window.isOpen())return -1;
	sf::Mouse mouse;

	const float targettime = 1.0f / 60.0f;
	float accumulator = 0.0f;
	float frametime = 0.0f;

	sf::Clock clock;

	/*sf::RectangleShape line(sf::Vector2f(150, 5));
	line.rotate(145);
	line.setPosition(120, 800);

	sf::RectangleShape line1(sf::Vector2f(150, 5));
	line1.rotate(165);
	line1.setPosition(256, 760);

	sf::RectangleShape line2(sf::Vector2f(150, 5));
	line2.rotate(175);
	line2.setPosition(400, 745);

	sf::RectangleShape line3(sf::Vector2f(150, 5));
	line3.rotate(180);
	line3.setPosition(550, 745);

	sf::RectangleShape line4(sf::Vector2f(150, 5));
	line4.rotate(180);
	line4.setPosition(620, 745);

	sf::RectangleShape line5(sf::Vector2f(150, 5));
	line5.rotate(185);
	line5.setPosition(770, 760);

	sf::RectangleShape line6(sf::Vector2f(150, 5));
	line6.rotate(195);
	line6.setPosition(915, 800);

	sf::RectangleShape line7(sf::Vector2f(150, 5));
	line7.rotate(205);
	line7.setPosition(1020, 850);
*/
	
	////enemy
	//sf::VertexArray enemyline(sf::Lines, 2);
	//enemyline[0].position = sf::Vector2f(50,50);
	//enemyline[1].position = sf::Vector2f(100, 50);
	//
	//

	sf::RectangleShape rect2;
	rect2.setSize(sf::Vector2f(500, 180));
	rect2.setPosition(272, 820);

	sf::RectangleShape rect3;
	rect3.setSize(sf::Vector2f(300, 100));
	rect3.setPosition(0, 880);

	sf::RectangleShape rect4;
	rect4.setSize(sf::Vector2f(300, 100));
	rect4.setPosition(770, 880);

	sf::RectangleShape rect5;
	rect5.setSize(sf::Vector2f(100, 30));
	rect5.setPosition(172, 850);

	sf::RectangleShape rect6;
	rect6.setSize(sf::Vector2f(100, 30));
	rect6.setPosition(772, 850);



	////world
	//sf::VertexArray line(sf::Lines, 6);

	//line[0].position = sf::Vector2f( 0, 600);
	//line[1].position = sf::Vector2f(300, 852);
	//line[2].position = sf::Vector2f(300, 852);
	//line[3].position = sf::Vector2f(933, 552);
	//line[4].position = sf::Vector2f(933, 552);
	//line[5].position = sf::Vector2f(1280, 600);

	

	

	while (window.isOpen())
	{
		sf::Time deltatime = clock.restart();
		frametime = std::min(deltatime.asSeconds(), 0.1f);
		if (frametime > 0.1f)
			frametime = 0.1f;

		accumulator += frametime;
		while (accumulator > targettime)
		{
			accumulator -= targettime;

			sf::Event event;
			while (window.pollEvent(event))
			{
				switch (event.type)
				{
				case sf::Event::KeyReleased:
					switch (event.key.code)
					{
					case sf::Keyboard::Up:
						menu.MoveUp();
						break;

					case sf::Keyboard::Down:
						menu.MoveDown();
						break;

					case sf::Keyboard::Return:
						switch (menu.GetPressedItem())
						{
						case 0:
							std::cout << "Play button has been pressed" << std::endl;
							break;
						case 1:
							std::cout << "Option button has been pressed" << std::endl;
							break;
						case 2:
							window.close();
							break;
						}

						break;
					}

					break;
				case sf::Event::Closed:
					window.close();

					break;

				}
				/*if (event.type == sf::Event::Closed) 
				{
					window.close();
					
				}*/
			}
		}
		window.clear(sf::Color(0x11, 0x22, 0x33, 0xff));

		std::cout << mouse.getPosition(window).x << " " << mouse.getPosition(window).y << std::endl;

		menu.draw(window);
		window.draw(rect2);
		window.draw(rect3);
		window.draw(rect4);
		window.draw(rect5);
		window.draw(rect6);
	
	
		window.display();
	}
	return 0;
}
#pragma once

#include "Incl.h"

class Planet
{
public:
	Planet();

	sf::RectangleShape rectOver;
	sf::RectangleShape rectNeder;

	int getMarketValue();
	void setMarketValue(int marketvalue);

private:
	int newMarketValue;

};


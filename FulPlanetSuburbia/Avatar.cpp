#include "Avatar.h"

Avatar::Avatar()
{
	spriteOver.setPosition(512, 920); 
	// Mitten av sk�rmen: 512, 500   Sprite storlekar: 120 120 75 150
	spriteOver.setOrigin(37.5, 75);
	spriteNeder.setPosition(452, 860);
}
float Avatar::getRotation(sf::RenderWindow &window)
{
	sf::Vector2i mouse = sf::Mouse::getPosition(window);
	sf::Vector2f CannonPos;
	CannonPos.x = 512;
	CannonPos.y = 920;

	const float PI = 3.14159;

	float dx = mouse.x - CannonPos.x;
	float dy = mouse.y - CannonPos.y;
	float rotation = (atan2(dy, dx)) * 180 / PI;

	return rotation + 90;
}
void Avatar::LookMouse(sf::RenderWindow &window)
{
	float rotation = getRotation(window);
	spriteOver.setRotation(rotation);
}
#pragma once

#include "Incl.h"

using namespace std;

class EMP
{
public:
	sf::CircleShape circleEMP;
	sf::Sprite spriteEMP;

	void Activate(sf::RenderWindow &window);
	EMP();

	void setActivateEMP(bool EMPActivated);
	void setObtainedEMP(bool EMPObtained);
	void setCX(float x);
	void setCY(float y);
	void setCR(float r);

	bool getActivateEMP();
	bool getObtainedEMP();
	float getCX();
	float getCY();
	float getCR();

private:
	bool newActivateEMP;
	bool newObtainedEMP;
	float newCX;
	float newCY;
	float newCR;
};
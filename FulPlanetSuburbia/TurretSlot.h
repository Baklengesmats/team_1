#pragma once

#include "Incl.h"

using namespace std;

class TurretSlot {
public:

	TurretSlot(sf::RenderWindow &window);

	sf::RectangleShape rect;


	void setFilled(bool);
	bool getFilled();

private:
	bool newFilled;
};

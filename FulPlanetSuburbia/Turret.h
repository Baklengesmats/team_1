#pragma once

#include "Incl.h"

class Turret {
public:
	sf::Sprite spriteOver;
	sf::Sprite spriteNeder;
	sf::RectangleShape rect;
	sf::Vector2f mousePos;
	float rotation;

	Turret();
	void movement(bool turn);

	int getTurnCounterTM();
	bool getTurnTM();
	float getRotationTM();

	void setTurnCounterTM(int counterTM);
	void setTurnTM(bool counterTM);
	void setRotationTM(float rotationTM);

private:
	int newTurnCounterTM;
	bool newTurnTM;
	float newRotationTM;

};
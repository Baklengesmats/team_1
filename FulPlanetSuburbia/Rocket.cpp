#include "Rocket.h"

Rocket::Rocket(sf::RenderWindow &window)
{
	rectRocket.setSize(sf::Vector2f(20, 60));
	rectRocket.setFillColor(sf::Color::Green);
	mousePos = window.mapPixelToCoords(sf::Mouse::getPosition(window));
}

void Rocket::movementFromAvatar(sf::RenderWindow &window)
{
	rectRocket.setRotation(rotation);
	spriteRocket.setRotation(rotation);
	spriteRocket.setPosition(rectRocket.getPosition());

	angle = atan2((mousePos.y - rectRocket.getGlobalBounds().height / 2) - 860,
		(mousePos.x - rectRocket.getGlobalBounds().width / 2) - 497);
	rectRocket.move(cos(angle)*(movementSpeed), sin(angle)*(movementSpeed));

	if (rectRocket.getPosition().y < -60)
	{
		destroy = true;
		cout << "rocket destroyed" << endl;
	}

}
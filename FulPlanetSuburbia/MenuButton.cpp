#include "MenuButton.h"



MenuButton::MenuButton()
{
	rect.setSize(sf::Vector2f(300, 100));
	rect.setFillColor(sf::Color::Green);
	newActive = true;
}

int MenuButton::getNumber()
{
	return newNumber;
}
void MenuButton::setNumber(int number)
{
	newNumber = number;
}

bool MenuButton::getActive()
{
	return newActive;
}
void MenuButton::setActive(bool active)
{
	newActive = active;
}
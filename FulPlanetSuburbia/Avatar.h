#pragma once
#include "Incl.h";

class Avatar {
public:
	sf::Sprite spriteOver;
	sf::Sprite spriteNeder;

	Avatar();
	float getRotation(sf::RenderWindow &window);
	void LookMouse(sf::RenderWindow &window);
};
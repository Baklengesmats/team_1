#include <SFML\Graphics.hpp>
#include <SFML\Main.hpp>
#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include "Enemy.h"


#include <iostream>
#include <vector>
using namespace std;

int main()
{
	// create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "My window");
	
	sf::Texture texture;
	if (!texture.loadFromFile("alien_hominid_styled_luigi__sheet__by_mamamia64-d5wgzj6.png"))
	{
		cout << "error loading the file" << endl;
	}
	
	sf::Sprite sprite(texture);
	sprite.setTexture(texture);
	sprite.setTextureRect(sf::IntRect(0, 0, 36, 64));
	
	const float targettime = 1.0f / 60.0f;
	float accumulator = 0.0f;
	float frametime = 0.0f;

	sf::Clock clock;


	while (window.isOpen())
	{
		sf::Time deltatime = clock.restart();
		frametime = std::min(deltatime.asSeconds(), 0.1f);
		if (frametime > 0.1f)
			frametime = 0.1f;

		accumulator += frametime;
		while (accumulator > targettime)
		{
			accumulator -= targettime;

			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed) {
					window.close();
					cout << "You closed the window";
				}
			}
		}
		window.clear();
		
		window.draw(sprite);
		
		sprite.move(0, 0.1);
		// end the current frame
		window.display();
	}

	return 0;
}
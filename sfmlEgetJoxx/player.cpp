#include "player.h"

player::player()
{
	rect.setSize(sf::Vector2f(32, 32));
	rect.setPosition(400, 200);
	rect.setFillColor(sf::Color::Blue);
	
}

void player::update()
{
	rect.setPosition(rect.getPosition());
}

void player::updateMovement()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		rect.move(0, -movementSpeed);
		direction = 1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		rect.move(0, movementSpeed);
		
		direction = 2;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		rect.move(-movementSpeed, 0);
		
		direction = 3;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		rect.move(movementSpeed, 0);
		
		direction = 4;
	}

	counterWalking++;

	if (counterWalking == 2)
	{
		counterWalking = 0;
	}
}
#pragma once
#include "entity.h"
#include "random.h"

#ifndef enemy_h
#define enemy_h
class enemy: public entity
{
public:
	enemy();
	~enemy();
	double movementSpeed = 0.1;
	int movementLength = 100;
	int attackDamage = 5;
	int counterWalking = 0;
	int direction = 0; // 1 - up, 2 - down, 3 - left, 4 - right
	int counter = 0;
	int hp = 1;
	bool alive = true;
	int destroyed = 0;
	int enemyLifeTime = 1000;



	void update();
	void updateMovement();



};

#endif
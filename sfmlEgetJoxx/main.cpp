#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <iostream>
#include <time.h>

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif

#include "random.h"
#include "player.h"
#include "entity.h"
#include "enemy.h"
#include "projectile.h"
#include "entity.h"
#include "game_state.h"


using namespace std;

game_state coreState;
bool quitGame = false;

int main(int argc, char** argv)
{

	sf::Clock clock;
	sf::Clock clock2;
	sf::Clock clock3;
	sf::Time time = clock.getElapsedTime();
	sf::Time timerestart = clock.restart();

	float playermovmentspeed = 2;
	int counter = 0;
	float timer = 0;
	int counter2 = 0;
	int counter3 = 0;
	

	sf::RenderWindow window;
	window.create(sf::VideoMode(1024, 1000), "sfml-app", sf::Style::Titlebar | sf::Style::Close);
	if (!window.isOpen())return -1;
	sf::View view(sf::FloatRect(0, 0, 1024, 1000));
	window.setView(view);

	sf::Texture textureEnemy;
	if (!textureEnemy.loadFromFile("Alienskepp 1.png")) 
	{
		std::cout << "error couldn't texturEnemy" << std::endl;
	}

	sf::Sprite spriteEnemy(textureEnemy);
	spriteEnemy.setPosition(window.getSize().x / 2, window.getSize().y / 2);
	spriteEnemy.setTextureRect(sf::IntRect(0, 0, 50, 30));

	

	

	
	
	class player Player1;
	
	
	
	vector<projectile>::const_iterator iter;
	vector<projectile> projectileArray;

	// Projectile Object
	class projectile projectile1;



	// Enemy Vector Array
	vector<enemy>::const_iterator iter4;
	vector<enemy> enemyArray;


	
	// Enemy Object
	enemy enemy1;
	enemy1.sprite.setTexture(textureEnemy);
	//enemy1.sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));

	enemy1.rect.setPosition(600, 0);
	enemyArray.push_back(enemy1);



	const float targettime = 1.0f / 60.0f;
	float accumulator = 0.0f;
	float frametime = 0.0f;

	/////world 
	//sf::ConvexShape convex;

	//// resize it to 5 points
	//convex.setPointCount(6);

	//// define the points
	//convex.setPoint(0, sf::Vector2f(0, 1000));
	//convex.setPoint(1, sf::Vector2f(155, 900));
	//convex.setPoint(2, sf::Vector2f(400, 870));
	//convex.setPoint(3, sf::Vector2f(700, 870));
	//convex.setPoint(4, sf::Vector2f(900, 900));
	//convex.setPoint(5, sf::Vector2f(1024, 1000));

	sf::RectangleShape rect2;
	rect2.setSize(sf::Vector2f(500, 180));
	rect2.setPosition(272, 760);

	sf::RectangleShape rect3;
	rect3.setSize(sf::Vector2f(300, 100));
	rect3.setPosition(0, 800);

	sf::RectangleShape rect4;
	rect4.setSize(sf::Vector2f(300, 100));
	rect4.setPosition(770, 800);

	sf::RectangleShape rect5;
	rect5.setSize(sf::Vector2f(100, 30));
	rect5.setPosition(172, 850);

	sf::RectangleShape rect6;
	rect6.setSize(sf::Vector2f(100, 30));
	rect6.setPosition(772, 800);
	
	
	while (window.isOpen())
	{
		timer++;
		sf::Time deltatime = clock.restart();
		frametime = std::min(deltatime.asSeconds(), 0.1f);
		if (frametime > 0.1f)
			frametime = 0.1f;

		accumulator += frametime;
		while (accumulator > targettime)
		{
			accumulator -= targettime;

			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed) {
					window.close();

				}
				
			}
		}
		window.clear(sf::Color(0x11, 0x22, 0x33, 0xff));

		sf::Time elapsed1 = clock2.getElapsedTime();
		sf::Time elapsed2 = clock3.getElapsedTime();
		counter = 0;
		for (iter = projectileArray.begin(); iter != projectileArray.end(); iter++)
		{
			counter2 = 0;
			for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
			{
				if (projectileArray[counter].rect.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
				{
					projectileArray[counter].Destroy = true;

					enemyArray[counter2].hp -= projectileArray[counter].attackDamage;
					if (enemyArray[counter2].hp <= 0)
					{
						enemyArray[counter2].alive = false;
					}
					/*else if(rect2.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
					{

						enemyArray[counter2].alive = false;
					}*/

				}

				counter2++;
			}

			counter++;
		}

		counter2 = 0;
		for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
		{
		if (rect2.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
			{

					enemyArray[counter2].alive = false;
			}
		else if (rect3.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
		{

			enemyArray[counter2].alive = false;
		}
		else if (rect4.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
		{

			enemyArray[counter2].alive = false;
		}
		else if (rect5.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
		{

			enemyArray[counter2].alive = false;
		}
		else if (rect6.getGlobalBounds().intersects(enemyArray[counter2].rect.getGlobalBounds()))
		{

			enemyArray[counter2].alive = false;
		}

			counter2++;
		}
		// Delete Dead Enemy
		counter = 0;
		for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
		{
			if (enemyArray[counter].alive == false)
			{
				cout << "Enemy has been destroyed" << endl;
				enemyArray.erase(iter4);
				break;
			}

			counter++;
		}

		// Delete Projectile
		counter = 0;
		for (iter = projectileArray.begin(); iter != projectileArray.end(); iter++)
		{
			if (projectileArray[counter].Destroy == true)
			{
				projectileArray.erase(iter);
				break;
			}

			counter++;
		}


		if (elapsed1.asMilliseconds() >=100 )
		{
			clock2.restart();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				projectile1.rect.setPosition(Player1.rect.getPosition().x + Player1.rect.getSize().x / 2 - projectile1.rect.getSize().x / 2, Player1.rect.getPosition().y + Player1.rect.getSize().y / 2 - projectile1.rect.getSize().y / 2);
				projectile1.direction;
				projectileArray.push_back(projectile1);
			}
		}
		
		if (timer>512)
		{
			enemy1.rect.setPosition(generateRandom(window.getSize().x),0);
			enemyArray.push_back(enemy1);
			timer = 0;
			
		}
		
		counter = 0;
		for (iter = projectileArray.begin(); iter != projectileArray.end(); iter++)
		{
			projectileArray[counter].update(); // Update Projectile
			window.draw(projectileArray[counter].rect);

			counter++;
		}

		counter = 0;
		for (iter4 = enemyArray.begin(); iter4 != enemyArray.end(); iter4++)
		{
			enemyArray[counter].update();
			enemyArray[counter].updateMovement();
			//window.draw(enemyArray[counter].rect);
			window.draw(enemyArray[counter].sprite);

			counter++;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
			view.rotate(1);

		Player1.update();
		Player1.updateMovement();

		window.draw(Player1.rect);
		window.draw(rect2);
		window.draw(rect3);
		window.draw(rect4);
		window.draw(rect5);
		window.draw(rect6);
		
		window.display();
	}
	return 0;
}
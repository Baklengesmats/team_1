#pragma once

#include "entity.h"

class projectile : public entity
{
public:
	int movementSpeed = 10;
	int attackDamage = 5;
	int direction = 1; // 1 - up, 2 - down, 3 - left, 4 - right
	bool Destroy = false;
	int counterLifetime = 0;
	int lifeTime = 100;

	projectile();
	~projectile();
	void update();
	//void updateMovement();
};
